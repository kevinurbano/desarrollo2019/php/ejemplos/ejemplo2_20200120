<?php
    session_start();
    $accion = "inicio";
    $accion(); // carga la funcion que se llama inicio

    function render($vista){
        require_once "./vistas/" . $vista . ".php";
    }
    
    function inicio(){
        render("inicio");
    }
    
    function suma(){
        render("suma");
    }
    
    function resta(){
        render("resta");
    }
       
?>


